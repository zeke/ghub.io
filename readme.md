# ghub.io

The code behing [ghub.io](http://ghub.io/).

## What

Redirect to a <a href="http://npmjs.org">npm</a> package's <a href="https://github.com">GitHub</a> page, if available.

Usage   : http://ghub.io/<strong>&lt;package-name&gt;</strong>

Example : <a href="http://ghub.io/review">http://ghub.io/review</a>

## License

(MIT)
